"""
Shows how to retrieve postcodes from geoloc data, and group the lessons by department

Requirements:
 - package google-cloud-bigquery must be installed
 - package requests must be installed
"""

from google.cloud import bigquery
import sys
import json
import requests

# TO BE CHANGED
serviceAccountKey = 'C:/Users/antoi/Documents/perso/ornikar/case-study/key/test-data-engineer-090621-ce3741f231ce.json'
outputFile = 'C:/Users/antoi/Documents/perso/ornikar/case-study/out/Q1b.json'

client = bigquery.Client.from_service_account_json(serviceAccountKey)

if __name__ == '__main__':

    # Get the script arguments
    partnershipTypes = sys.argv[1].replace(',', '","')
    startDate = sys.argv[2]
    endDate = sys.argv[3]

    # Get the number of lessons grouped by latitude and longitude
    QUERY = (
        'SELECT m.mp_latitude, m.mp_longitude, COUNT(*) AS nb_lessons '
        'FROM `test-data-engineer-090621.test_dataset.bookings` b '
        'INNER JOIN `test-data-engineer-090621.test_dataset.lessons` l ON l.Lesson_id=b.Lesson_id '
        'INNER JOIN `test-data-engineer-090621.test_dataset.instructors` i ON i.instructor_id=l.Instructor_id '
        'INNER JOIN `test-data-engineer-090621.test_dataset.meeting_points` m ON m.meeting_point_id=l.Meeting_point_id '
        'WHERE b.Booking_deleted_at IS NULL '
        'AND l.lesson_deleted_at IS NULL '
        'AND i.partnership_type IN ("' + partnershipTypes + '") '
        'AND l.Lesson_start_at BETWEEN TIMESTAMP("' + startDate + '") AND TIMESTAMP("' + endDate + '") '
        'GROUP BY m.mp_latitude, m.mp_longitude '
        'ORDER BY nb_lessons DESC')

    # Run the query
    query_job = client.query(QUERY)

    # Get the query resluts
    rows = query_job.result()

    # Process lines : call the geocoding reverse API for each line and group by department
    data = {}
    for row in rows:
        urlQueryParams = {'lon': row['mp_longitude'], 'lat': row['mp_latitude']}
        response = requests.get("https://api-adresse.data.gouv.fr/reverse", params=urlQueryParams)
        postcode = response.json()['features'][0]['properties']['postcode'][0:2]
        if postcode in data:
            data[postcode] = data[postcode] + row['nb_lessons']
        else:
            data[postcode] = row['nb_lessons']
        print(postcode + ": " + str(data[postcode]))

    # Write the output
    with open(outputFile, 'w') as outfile:
        json.dump(data, outfile)
