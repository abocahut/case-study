## Introduction
In this project we will see how to query the dataset of lessons provided by Ornikar, in order to get insights from it, and how to enrich this dataset through external API's and AI models.

---

## Q1a
We want to get the 5 best instructors on 2020 Q3, and for those instructors, the date of their 50th lesson. For that purpose, we will use SQL windowing functions to count the total of lessons per instructor, and to filter the lines on the 50th lesson only.
#### Query
 ````
 SELECT
  i.instructor_id,
  l.Lesson_start_at,
  COUNT(*) OVER (PARTITION BY i.instructor_id) AS nb_lessons
FROM
  `test-data-engineer-090621.test_dataset.bookings` b
INNER JOIN
  `test-data-engineer-090621.test_dataset.lessons` l
ON
  l.Lesson_id=b.Lesson_id
INNER JOIN
  `test-data-engineer-090621.test_dataset.instructors` i
ON
  i.instructor_id=l.Instructor_id
WHERE
  b.Booking_deleted_at IS NULL
  AND l.lesson_deleted_at IS NULL
  AND l.Lesson_start_at BETWEEN TIMESTAMP("2020-07-01 00:00:00+00") AND TIMESTAMP("2020-09-30 23:59:59+00") 
QUALIFY ROW_NUMBER() OVER (PARTITION BY i.instructor_id ORDER BY Lesson_start_at) = 50
ORDER BY
  nb_lessons DESC
LIMIT 5;
 ````
#### Results
![](assets/Q1a.png)

## Q1b
We will write a python script that extracts lessons from our BigQuery database, based on parameters, and returns a JSON file containing the number of lessons grouped by department.
Our dataset contains only meeting points identified by geolocation information, so we need to reverse code those geolocations in order to get the full addresses, and especially the postcodes. From the postcodes, we will then be able to guess the departments. 
For this reverse geocoding process, we will use an opendata API provided by the french government : [adress API](https://geo.api.gouv.fr/adresse)

#### Requirements

We need to install the two following packages :

```
pip install google-cloud-bigquery
pip install requests
```

#### Run the script

You can run the script by calling the `Q1b.py` file with 3 mandatory parameters :

1. **partnership types** : a coma-separated list of values
2. **start date** : the oldest date of lessons we want to process
3. **end date** : the newest date of lessons we want to process

```
py Q1b.py "EI,ME" "2020-01-01" "2020-12-31"
```
The output will be a JSON file located in `/out/Q1b.json`. The file already provided is the result of the previous run command.

Before you run the script for the first time, you must change the **serviceAccountKey** and **outputFile** constants

## Q2a

In order to ingest a huge amount of data in real time, we can use GCP Dataflow service. This is a streaming ETL platorm, serverless and highly scalable. We can use Pub/Sub as an event notification service, that will be triggered by the webapp or the mobile app, and then consumed by dataflow in order tu push the data into BiqQuery.

![](assets/Q2a.png)

## Q2b

Now we want to expose an AI model to tag each message before inserting it into the database. We can use the Vertex AI service, which will allow us to expose a model developed with TenserFlow through http REST API's. Our dataflow job will call that endpoint, retrieve the results and then write the tagged lesson into BigQuery.

![](assets/Q2b.png)

---
